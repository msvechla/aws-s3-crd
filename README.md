# Simple AWS S3 Kubernetes Custom Resource Definition

This implements a simple Kubernetes CRD (Custom Resource Definition), which allows creating a S3 bucket on AWS based on a Kubernetes yaml definition. This controller then takes care of the lifecycle of the bucket.

**DISCLAIMER:** This projects is still under heavy development and has the intention to become a starting point for developing your own CRD. It is **NOT intended for production purposes!**

## Purpose

The purpose of this project is purely educational and a small POC of how CRDs can enable developers to use Kubernetes as an abstraction layer for the entire infrastructure. This hints at a future where *Kubernetes has become the interface to the cloud*.

## Usage Instructions

To get started, deploy the CRD to the cluster via `kubect apply -f artifacts/crd.yaml`.

Afterwards edit the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY_ID` in the [controller-deployment.yml](artifacts/controller-deployment.yml) and deploy it to your cluster via `kubectl apply -f artifacts/controller-deployment.yml`.

Creating the first S3 bucket is as easy as editing the `name` inside [example-s3.yaml](artifacts/example-s3.yaml) and deploying it to the cluster.

## References

- Based on the instructions from [kubernetes/sample-controller](https://github.com/kubernetes/sample-controller)
- Official [CRD Documentation](https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definitions/)