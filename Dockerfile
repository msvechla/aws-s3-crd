FROM golang:1.10 as builder
WORKDIR /go/src/gitlab.com/msvechla/aws-s3-crd
COPY ./ /go/src/gitlab.com/msvechla/aws-s3-crd/
RUN go get .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o aws-s3-crd .

FROM alpine:latest
RUN addgroup -g 1000 -S app && \
    adduser -u 1000 -S app -G app && \
    mkdir /app/
WORKDIR /app/
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
ENV AWS_REGION=eu-central-1 AWS_ACCESS_KEY_ID="" AWS_SECRET_ACCESS_KEY=""

COPY --from=builder /go/src/gitlab.com/msvechla/aws-s3-crd/aws-s3-crd .
RUN chown -R app /app/
USER app
ENTRYPOINT ["./aws-s3-crd"]
